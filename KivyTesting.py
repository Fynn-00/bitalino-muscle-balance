from kivy.lang import Builder
from kivymd.app import MDApp, ObjectProperty
from kivy.uix.screenmanager import Screen, ScreenManager, StringProperty
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout 
from kivy.uix.image import Image 
from kivy.cache import Cache
import shutil
import os


# Imports from BITalino sample script
import time
import numpy as np
from pylab import FigureCanvasBase, plot, xlim, show
from bitalino import BITalino
import matplotlib.pyplot as plt

from subprocess import call as subprocess_call
from os.path import isfile as file_exist

macAddress = "20:16:02:14:75:38"

batteryThreshold = 30
acqChannels = [0, 1]
samplingRate = 1000
nSamples = 15
digitalOutput_on = [1, 1]
digitalOutput_off = [0, 0]

device = BITalino(macAddress)

current_set_number = 0
current_part_number = 0

print(device.version())
device.battery(batteryThreshold)


KV = '''
ScreenManager:
    WelcomeScreen:
    StopScreen:
    ResultScreen:


<WelcomeScreen>:
    name: 'welcome'
    MDTextField:
        id: part_number
        hint_text: "Participant"
        icon_right: "account"
        pos_hint: {"center_x": .5, "center_y": .55}
        size_hint: (.5, .1)
        mode: "rectangle"

    MDTextField:
        id: set_number
        hint_text: "Set Number"
        pos_hint: {"center_x": .5, "center_y": .45}
        size_hint: (.5, .1)
        mode: "rectangle"

    MDFillRoundFlatButton:
        text: "GO!"
        font_size: 48
        pos_hint: {"center_x": .5, "center_y": .35}
        size_hint: (.5, .1)
        on_press: 
            root.manager.current = 'stop'
    MDFillRoundFlatButton:
        text: "Exit"
        font_size: 48
        pos_hint: {"center_x": .5, "center_y": .15}
        size_hint: (.5, .1)
        on_press: 
            root.manager.current = 'exit'


<StopScreen>:
    name: 'stop'
    MDFillRoundFlatButton:
        text: 'STOP!'
        font_size: 48
        pos_hint: {'center_x': 0.5, 'center_y': 0.5}
        size_hint: (1, 1)
        on_press: 
            root.manager.current = 'results'
 
<ResultScreen>:
    name: 'results'
    img: preview
    BoxLayout:
        orientation: "vertical"
        AsyncImage:
            id: preview
            nocache: True
        MDFillRoundFlatButton:
            text: "Restart"
            font_size: 48
            pos_hint: {"center_x": .5, "center_y": .15}
            on_press: 
                root.manager.current = 'welcome'

<ExitScreen>:
    name: 'exit'
    MDFillRoundFlatButton:
        text: "Restart"
        font_size: 48
        pos_hint: {"center_x": .5, "center_y": .15}
        on_press: 
            root.manager.current = 'welcome'
'''
class WelcomeScreen(Screen):
    def on_leave(self):
        current_part_number = self.ids.part_number.text
        print("Participant: " + current_part_number)
        current_set_number = self.ids.set_number.text
        print("Set: " + current_set_number)
        global file
        global figName
        global name
        global readBufferName

        figName = "part{}_set{}.png".format(current_part_number,current_set_number)
        name = "part{}_set{}.txt".format(current_part_number,current_set_number)
        readBufferName = "part{}_set{}-buffer.txt".format(current_part_number,current_set_number)

        if not file_exist(name):
            subprocess_call(["touch", name])
            subprocess_call(["chmod", "777", name])
        else:
           raise Exception("File " + name + " already exists") 
        file = open(name, "a+")
    pass

class StopScreen(Screen):
    def collectData(self,dt):
        read = device.read(nSamples)
        np.savetxt(readBufferName, read, fmt='%d')
        readBuffer = open(readBufferName, "r")
        readBufferData = readBuffer.read()
        file.write(readBufferData)
        #device.trigger(digitalOutput_on)

    def on_enter(self):
        device.start(samplingRate, acqChannels)
        Clock.max_iteration = 1000
        Clock.schedule_interval(self.collectData, 0.01)
        print("Start")

    def on_leave(self):
        print("Stop")
        Clock.unschedule(self.collectData)
        device.stop()

        # Read TXT
        data = np.loadtxt(name, int)
        leftData = data[:, 5]
        rightData = data[:, 6]

        ## Calculate and print Average/Median/Peak
        print("left Average: ")
        print(np.average(leftData))
        print("right Average: ")
        print(np.average(rightData))
        print("average Difference: ")
        print(leftData - rightData)

        print("left Median: ")
        print(np.median(leftData))
        print("right Median: ")
        print(np.median(rightData))
        print("median Difference")
        print(np.median(leftData) - np.median(rightData))

        print("left Peak: ")
        print(np.max(leftData))
        print("right Peak: ")
        print(np.max(rightData))
        print("Peak difference: ")
        print(np.max(leftData) - np.max(rightData))

        leftX = np.arange(0, leftData.size)
        leftY = np.array(leftData)
        x = np.arange(0, leftData.size)
        y_mean2 = [np.mean(rightData)] * len(x)
        y_mean1 = [np.mean(leftData)] * len(x)

        rightX = np.arange(0, rightData.size)
        rightY = np.array(rightData)

        plt.plot(leftX, leftY)
        plt.plot(rightX, rightY)
        plt.plot(leftX, y_mean1)
        plt.plot(rightX, y_mean2)
        
        plt.title("Left vs Right arm tension")
        plt.xlabel("Time")
        plt.ylabel("Tension")
        if not file_exist(figName):
            plt.savefig(figName, dpi=300)
        else:
            raise Exception("Figure " + figName + " already exists")

        shutil.copyfile(figName, "preview.png")

        file.close()
    pass

class ResultScreen(Screen):
    img = ObjectProperty()
    def on_enter(self):
        self.img.source = 'preview.png'
        self.img.reload()

class ExitScreen(Screen):
    def on_enter(self):
        device.close()


sm = ScreenManager()
sm.add_widget(WelcomeScreen(name='welcome'))
sm.add_widget(StopScreen(name='stop'))
sm.add_widget(ResultScreen(name='results'))
sm.add_widget(ExitScreen(name='exit'))

class MainApp(MDApp):
    def build(self):
        screen = Builder.load_string(KV)
        return screen

if __name__ == '__main__':
    MainApp().run()
