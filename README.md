# Bitalino Muscle Balance

Small Python program to read out EMG data from a BITalino

## Usage:
- Disclaimer - App was only tested under Linux - Disclaimer -
1. Clone the repo to your disk or Download and extract it as an archive
2. To run you will need the Python-Libraries: matplotlib, kivy, numpy and bitalino
3. Turn on the BITalino and pair it via Bluetooth (Two pairs of electrodes should be connected to port and attached to the musclegroups you want to measure)
4. Enter the Folder containing the Repository in a Terminal
5. Run ```python3 KivyTesting.py``` (A window should open)
6. Now put in your participant and set and press "Start" (BITalino will start measuring)
7. When you're done press "Stop" and you will a graph (see "Reading the Graph") 
9. To exit Press the "Exit" button on the main screen

## Reading the Graph:
- Orange is the graph of the electrodes data from port A2 (left body half in our testing)
- Blue is the graph of the electrode data from port A1 (right body half in our testing)
- Green is the average line of the Orange graph
- Red is the average line of the Blue Graph
- y axis shows the tension of the muscle
- x axis shows the amount of samples taken
